const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const app = express()


app.use(bodyParser.json())
app.use(cors())

require('./routes')(app);

app.set('port', process.env.PORT || 8081)
app.listen(process.env.PORT || 8081)
