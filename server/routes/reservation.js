var express = require('express');
var app = express();
const axios = require('axios')

module.exports = function(app){
  app.get('/api/rooms', (req, res) => {
    axios.get('https://online.stationf.co/tests/rooms.json')
      .then(response => {
        console.log(response.data);
        res.status(200)
        res.send({ rooms: response.data.rooms });
      })
      .catch(err =>{
        console.error(err);
        res.status(500)
        res.send({error: 'unable to fetch data'});
      }) 
  });

    //other routes..
}